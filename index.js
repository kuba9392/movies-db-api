const http = require('http');

const app = require('./app');
const logger = require('./logger');

const APPLICATION_PORT = +process.env.APPLICATION_PORT || 3000;
http.createServer(app).listen(APPLICATION_PORT, () => {
  logger.info(`API listening to port ${APPLICATION_PORT}`);
});
