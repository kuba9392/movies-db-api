const app = new (require('express'))();
const Container = require('typedi').Container;
const swaggerUi = require('swagger-ui-express');
const swaggerJsDoc = require('swagger-jsdoc');
const bodyParser = require('body-parser');
const log4js = require('log4js');

const moviesServices = require('./src/movies/services');
const MoviesRouter = require('./src/movies/movies.router');
const swaggerOptions = require('./config/swaggerOptions.json');
const logger = require('./logger');
const ErrorHandlerMiddleware = require('./src/shared/middlewares/error-handler.middleware');

moviesServices.init(Container);
const moviesRouter = Container.get(MoviesRouter);

const specs = swaggerJsDoc({
  ...swaggerOptions,
  apis: [
    './src/movies/entities/*.js',
    './src/movies/actions/*.js',
    './src/movies/dto/*.js',
    './src/movies/movies.router.js'
  ]
});

app.use(log4js.connectLogger(logger, { format: ':method :url :status' }));
app.use(bodyParser.json());
app.use(moviesRouter.init());
app.use(
  '/api-docs',
  swaggerUi.serve,
  swaggerUi.setup(specs, { explorer: true })
);
app.use((new ErrorHandlerMiddleware(logger)).invoke);

module.exports = app;
