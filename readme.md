## Movies Database API
Simple API to manage Movies JSON database.

### Requirements
- Node.js v14.15.1

### Running
Dependencies should be installed before running the application.

To run the application, use:
``` bash
    npm run start
```

Application is working on port 3000 by default, to change it set APPLICATION_PORT
environment variable.

### Swagger
After running application, it's possible to navigate through the endpoints with
Swagger, which is available at **/api-docs** endpoint.

### Tests
To run the tests, use
``` bash
    npm run test
