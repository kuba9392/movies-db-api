const DatabaseError = require('../errors/database.error');

class BaseRepository {
  /**
   * @param {JsonDatabase} database | Database object with connection to file
   * @param {BaseEntity} entity | Entity object for repository creation
   */
  constructor (database, entity) {
    this.database = database;
    this.entity = entity;
  }

  async find (predicate) {
    return this.database.select(this.entity.name, predicate);
  }

  async save (entity) {
    let row;
    Object.entries(this.entity.structure).forEach(([key, options]) => {
      if (key === '#') {
        if (typeof (entity.value) !== options.type) {
          throw new DatabaseError('Bad type of data for value field, should be ' + options.type);
        }

        row = entity.value;
        return;
      }

      row = this.#parseObjectEntity(entity, key, options, row);
    });

    return this.database.insert(this.entity.name, row);
  }

  #parseObjectEntity = (entity, key, options, row) => {
    if (typeof (entity[key]) !== options.type && !options.autoIncremented) {
      throw new DatabaseError('Bad type of data for ' + key + ' field, should be ' + options.type);
    }

    if (!row) {
      row = {};
    }

    if (options.primary) {
      row.primary = key;
    }

    if (options.autoIncremented && !row.autoIncremented) {
      row.autoIncremented = [];
    }

    if (options.autoIncremented) {
      row.autoIncremented.push(key);
    }

    row[key] = entity[key];
    return row;
  }
}

module.exports = BaseRepository;
