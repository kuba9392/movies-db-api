const BaseRepository = require('./base.repository');
const BaseEntity = require('../database/base.entity');
const JsonDatabase = require('../database/json.database');

const mockFs = require('mock-fs');

class TestEntity extends BaseEntity {
  constructor (value) {
    const structure = {
      '#': { type: 'string' }
    };

    super('testResource', structure);

    this.value = value;
  }
}

class TestObjectEntity extends BaseEntity {
  constructor (testTitle, testProp) {
    const structure = {
      testTitle: { type: 'string', primary: true },
      testProp: { type: 'number' }
    };

    super('testObjectResource', structure);

    this.testTitle = testTitle;
    this.testProp = testProp;
  }
}

class TestAutoIncrementedEntity extends BaseEntity {
  constructor (testTitle, testProp) {
    const structure = {
      id: { type: 'number', primary: true, autoIncremented: true },
      testTitle: { type: 'string' },
      testProp: { type: 'number' }
    };

    super('testAutoIncrementedResource', structure);

    this.id = 0;
    this.testTitle = testTitle;
    this.testProp = testProp;
  }
}

describe('BaseRepository', () => {
  const filePath = 'testDb.json';
  const dbData = {
    testResource: ['testValue1', 'testValue2'],
    testObjectResource: [{ testTitle: 'test', testProp: 2 }],
    testAutoIncrementedResource: [{ id: 1, testTitle: 'test', testProp: 2 }]
  };

  let repository;
  let objectRepository;
  let autoIncrementedRepository;

  beforeAll(() => {
    mockFs({
      [filePath]: JSON.stringify(dbData)
    });

    const database = new JsonDatabase(filePath);
    const entity = new TestEntity();
    const objectEntity = new TestObjectEntity();
    const autoIncrementedEntity = new TestAutoIncrementedEntity();

    repository = new BaseRepository(database, entity);
    objectRepository = new BaseRepository(database, objectEntity);
    autoIncrementedRepository = new BaseRepository(database, autoIncrementedEntity);
  });

  afterAll(() => {
    mockFs.restore();
  });

  describe('find method', () => {
    it('SHOULD find records by given predicate', async () => {
      const result = await repository.find(entity => entity === 'testValue1');

      expect(result).toHaveLength(1);
      expect(result[0]).toEqual('testValue1');
    });

    it('SHOULD find all records WHEN predicate is not declared', async () => {
      const result = await repository.find();

      expect(result).toHaveLength(2);
      expect(result[0]).toEqual('testValue1');
      expect(result[1]).toEqual('testValue2');
    });
  });

  describe('save method', () => {
    it('SHOULD add new entity WHEN entity does not exists in the resource', async () => {
      const entity = new TestEntity('testValue3');
      await repository.save(entity);
      const result = await repository.find(entity => entity === 'testValue3');

      expect(result).toHaveLength(1);
      expect(result[0]).toEqual('testValue3');
    });

    it('SHOULD not add new entity WHEN entity does exists in the resource', async () => {
      const entity = new TestEntity('testValue3');
      await repository.save(entity);
      await repository.save(entity);

      const result = await repository.find(entity => entity === 'testValue3');

      expect(result).toHaveLength(1);
      expect(result[0]).toEqual('testValue3');
    });

    it('SHOULD throw an error when value is with bad type', async () => {
      const entity = new TestEntity(5);

      let errorMessage;
      try {
        await repository.save(entity);
      } catch (err) {
        errorMessage = err.message;
      }

      expect(errorMessage).toEqual('Bad type of data for value field, should be string');
    });

    it('SHOULD add new objectEntity WHEN entity does not exists in the resource', async () => {
      const entity = new TestObjectEntity('title1', 3);
      await objectRepository.save(entity);
      const result = await objectRepository.find(entity => entity.testTitle === 'title1');

      expect(result).toHaveLength(1);
      expect(result[0].testTitle).toEqual('title1');
      expect(result[0].testProp).toEqual(3);
    });

    it('SHOULD not add new objectEntity WHEN entity does exists in the resource', async () => {
      const entity = new TestObjectEntity('title2', 3);
      await objectRepository.save(entity);
      await objectRepository.save(entity);
      const result = await objectRepository.find(entity => entity.testTitle === 'title2');

      expect(result).toHaveLength(1);
      expect(result[0].testTitle).toEqual('title2');
      expect(result[0].testProp).toEqual(3);
    });

    it('SHOULD throw an error when value is with bad type in objectEntity', async () => {
      const entity = new TestObjectEntity('title2', 'test');

      let errorMessage;
      try {
        await objectRepository.save(entity);
      } catch (err) {
        errorMessage = err.message;
      }

      expect(errorMessage).toEqual('Bad type of data for testProp field, should be number');
    });

    it('SHOULD add new TestAutoIncremented entity with new id WHEN id has autoIncremented option set to true', async () => {
      const entity = new TestAutoIncrementedEntity('title2', 3);
      await autoIncrementedRepository.save(entity);
      const result = await autoIncrementedRepository.find(entity => entity.testTitle === 'title2');

      expect(result).toHaveLength(1);
      expect(result[0].id).toEqual(2);
      expect(result[0].testTitle).toEqual('title2');
      expect(result[0].testProp).toEqual(3);
    });
  });
});
