class BaseAction {
  constructor (method, route, middlewares) {
    this.method = method;
    this.route = route;
    this.middlewares = middlewares;
  }

  async execute (req, res) {
    throw Error('Method not implemented');
  }
}

module.exports = BaseAction;
