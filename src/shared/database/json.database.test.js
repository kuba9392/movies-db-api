const JsonDatabase = require('./json.database');
const mockFs = require('mock-fs');

describe('JsonDatabase', () => {
  let database;

  beforeAll(() => {
    const filePath = 'testDb.json';
    const data = { entity: ['testValue1', 'testValue2'], objectEntity: [{ id: 1, prop: 'value' }] };

    mockFs({
      'testDb.json': JSON.stringify(data)
    });

    database = new JsonDatabase(filePath);
  });

  afterAll(() => {
    mockFs.restore();
  });

  describe('select method', () => {
    it('SHOULD return all data from entity key WHEN predicate is not set', async () => {
      const result = await database.select('entity');

      expect(result).toHaveLength(2);
      expect(result[0]).toEqual('testValue1');
      expect(result[1]).toEqual('testValue2');
    });

    it('SHOULD return selected data from entity key WHEN predicate is set', async () => {
      const result = await database.select('entity', value => value === 'testValue1');

      expect(result).toHaveLength(1);
      expect(result[0]).toEqual('testValue1');
    });
  });

  describe('insert method', () => {
    it('SHOULD insert new row to entity', async () => {
      const result = await database.insert('entity', 'testValue3');

      expect(result).toEqual('testValue3');
    });

    it('SHOULD not insert new row to entity WHEN row already exists', async () => {
      await database.insert('entity', 'testValue3');
      await database.insert('entity', 'testValue3');

      const result = await database.select('entity', value => value === 'testValue3');
      expect(result).toHaveLength(1);
      expect(result[0]).toEqual('testValue3');
    });

    it('SHOULD auto increment value WHEN entity has autoIncremented array property', async () => {
      const entity = { prop: 'value2', autoIncremented: ['id'] };
      const result = await database.insert('objectEntity', entity);

      expect(result).toEqual({ id: 2, prop: 'value2' });
    });
  });
});
