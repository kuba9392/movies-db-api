const fs = require('fs').promises;

class JsonDatabase {
  constructor (filePath) {
    this.filePath = filePath;
  }

  /**
   * Select values from database
   * @name select
   * @function
   * @param {String} resource | Name of the resource in the database
   * @param {predicate} predicate | Function with predicate to filter database results
   */
  async select (resource, predicate) {
    const db = await this.#loadDatabase();

    if (!predicate) {
      return db[resource];
    };

    return db[resource].filter(predicate);
  }

  /**
   * Insert value to database
   * @name insert
   * @function
   * @param {String} resource | Name of the resource in the database
   * @param {Object} row | Value to insert to the resource in the database
   */
  async insert (resource, row) {
    const db = await this.#loadDatabase();

    if (row.primary && db[resource].find(entity => entity[row.primary] === row[row.primary])) {
      return;
    }

    if (db[resource].find(entity => entity === row)) {
      return;
    }

    if (row.autoIncremented && row.autoIncremented.length > 0) {
      row.autoIncremented.forEach(property => {
        row[property] = Math.max.apply(Math, db[resource].map(entity => { return entity[property]; })) + 1;
      });
    }

    delete row.autoIncremented;
    delete row.primary;

    db[resource].push(row);
    await fs.writeFile(this.filePath, JSON.stringify(db, null, 4));
    return row;
  }

  #loadDatabase = async () => {
    const file = await fs.readFile(this.filePath);
    return JSON.parse(file);
  }
}

module.exports = JsonDatabase;
