class BaseEntity {
  constructor (name, structure) {
    this.name = name;
    this.structure = structure;
  }
}

module.exports = BaseEntity;
