const BaseEntity = require('../../shared/database/base.entity');

class Genre extends BaseEntity {
  constructor () {
    const structure = {
      '#': { type: 'string' }
    };

    super('genres', structure);
  }
}

module.exports = Genre;
