const BaseEntity = require('../../shared/database/base.entity');

/**
 * @swagger
 * components:
 *  schemas:
 *    Movie:
 *      type: object
 *      required:
 *        - title
 *        - year
 *        - runtime
 *        - director
 *      properties:
 *        id:
 *          type: integer
 *        genres:
 *          type: array
 *          items:
 *            type: string
 *        title:
 *          type: string
 *        year:
 *          type: integer
 *        runtime:
 *          type: integer
 *        director:
 *          type: string
 *        actors:
 *          type: string
 *        plot:
 *          type: string
 *        posterUrl:
 *          type: string
 */
class Movie extends BaseEntity {
  constructor () {
    const structure = {
      id: { type: 'number', primary: true, autoIncremented: true },
      title: { type: 'string' },
      year: { type: 'string' },
      runtime: { type: 'string' },
      genres: { type: 'object' },
      director: { type: 'string' },
      actors: { type: 'string' },
      plot: { type: 'string' },
      posterUrl: { type: 'string' }
    };

    super('movies', structure);

    this.id = 0;
    this.title = '';
    this.year = '';
    this.runtime = '';
    this.genres = [];
    this.director = '';
    this.actors = '';
    this.plot = '';
    this.posterUrl = '';
  };

  static createFromDto (dto) {
    const movie = new Movie();

    movie.title = dto.title;
    movie.year = `${dto.year}`;
    movie.runtime = `${dto.runtime}`;
    movie.genres = dto.genres;
    movie.director = dto.director;
    movie.actors = dto.actors;
    movie.plot = dto.plot;
    movie.posterUrl = dto.posterUrl;

    return movie;
  }
}

module.exports = Movie;
