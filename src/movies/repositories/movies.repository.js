const BaseRepository = require('../../shared/repositories/base.repository');
const Movie = require('../entities/movie');

class MoviesRepository extends BaseRepository {
  constructor (container) {
    const database = container.get('moviesDatabase');
    const entity = new Movie();

    super(database, entity);
  }
}

module.exports = MoviesRepository;
