const BaseRepository = require('../../shared/repositories/base.repository');
const Genre = require('../entities/genre');

class GenresRepository extends BaseRepository {
  constructor (container) {
    const database = container.get('moviesDatabase');
    const entity = new Genre();

    super(database, entity);
  }
}

module.exports = GenresRepository;
