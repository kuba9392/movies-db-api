const router = require('express').Router();
const FindMoviesAction = require('./actions/find-movies.action');
const CreateMovieAction = require('./actions/create-movie.action');

/**
 * @swagger
 * tags:
 *   name: Movies
 *   description: Movies management
*/
class MoviesRouter {
  constructor (container) {
    this.actions = [
      container.get(FindMoviesAction),
      container.get(CreateMovieAction)
    ];
  }

  init () {
    this.actions.forEach(action => {
      if (action.middlewares) {
        router[action.method.toLowerCase()](action.route, ...action.middlewares, action.execute);
        return;
      }

      router[action.method.toLowerCase()](action.route, action.execute);
    });

    return router;
  }
}

module.exports = MoviesRouter;
