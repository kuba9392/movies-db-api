class MoviesUtils {
  static isBetweenDurationRange (movie, duration) {
    return (movie.runtime >= duration - 10) && (movie.runtime <= duration + 10);
  }

  static hasOneOfGenres (movie, genres) {
    return movie.genres.some(genre => genres.includes(genre));
  }

  static sortByNumberOfMatches (movies, genres) {
    return movies.sort((a, b) => {
      return b.genres.filter(genre => genres.includes(genre)).length - a.genres.filter(genre => genres.includes(genre)).length;
    });
  }
}

module.exports = MoviesUtils;
