const { Joi } = require('celebrate');

/**
   * @swagger
   * components:
   *  schemas:
   *    CreateMovie:
   *      type: object
   *      required:
   *        - title
   *        - year
   *        - runtime
   *        - director
   *      properties:
   *        genres:
   *          type: array
   *          items:
   *            type: string
   *        title:
   *          type: string
   *        year:
   *          type: integer
   *        runtime:
   *          type: integer
   *        director:
   *          type: string
   *        actors:
   *          type: string
   *        plot:
   *          type: string
   *        posterUrl:
   *          type: string
   */
class PostCreateMovieDto {
    static validation = Joi.object().keys({
      title: Joi.string().max(255).required(),
      year: Joi.number().required(),
      runtime: Joi.number().required(),
      genres: Joi.array().items(Joi.string()).required(),
      director: Joi.string().max(255).required(),
      actors: Joi.string().optional(),
      plot: Joi.string().optional(),
      posterUrl: Joi.string().optional()
    });

    constructor (body) {
      this.title = body.title;
      this.year = body.year;
      this.runtime = body.runtime;
      this.genres = body.genres;
      this.director = body.director;
      this.actors = body.actors || '';
      this.plot = body.plot || '';
      this.posterUrl = body.posterUrl || '';
    }
}

module.exports = PostCreateMovieDto;
