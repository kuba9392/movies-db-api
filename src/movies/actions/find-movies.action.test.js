const request = require('supertest');
const app = require('../../../app');

describe('GET /movies', () => {
  it('SHOULD respond with random movie WHEN no query parameters have been sent', (done) => {
    request(app)
      .get('/movies')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        expect(res.body).toHaveLength(1);

        checkMovieProperties(res.body[0]);
        done();
      });
  });

  it('SHOULD respond with random movie between duration range WHEN duration query parameter have been sent', (done) => {
    request(app)
      .get('/movies?duration=110')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        expect(res.body).toHaveLength(1);

        const movie = res.body[0];
        checkMovieProperties(movie);
        expect(+movie.runtime).toBeGreaterThanOrEqual(100);
        expect(+movie.runtime).toBeLessThanOrEqual(120);
        done();
      });
  });

  it('SHOULD respond with movies that contain genres WHEN genres have been provided in the request', (done) => {
    request(app)
      .get('/movies?genres=Comedy')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);

        res.body.forEach(movie => {
          checkMovieProperties(movie);
          expect(movie.genres).toContain('Comedy');
        });
        done();
      });
  });

  it('SHOULD respond with movies between duration range that contain genres WHEN genres and duration have been provided in the request', (done) => {
    request(app)
      .get('/movies?genres=Comedy&duration=110')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);

        res.body.forEach(movie => {
          checkMovieProperties(movie);
          expect(movie.genres).toContain('Comedy');
          expect(+movie.runtime).toBeGreaterThanOrEqual(100);
          expect(+movie.runtime).toBeLessThanOrEqual(120);
        });
        done();
      });
  });
});

function checkMovieProperties (movie) {
  expect(movie).toHaveProperty('id');
  expect(movie).toHaveProperty('title');
  expect(movie).toHaveProperty('year');
  expect(movie).toHaveProperty('runtime');
  expect(movie).toHaveProperty('genres');
  expect(movie).toHaveProperty('actors');
  expect(movie).toHaveProperty('plot');
  expect(movie).toHaveProperty('posterUrl');
}
