const BaseAction = require('../../shared/actions/base.action');
const FindRandomMovieStrategy = require('../strategies/find-random-movie.strategy');
const FindMovieByDurationStrategy = require('../strategies/find-movie-by-duration.strategy');
const FindMoviesByGenresStrategy = require('../strategies/find-movies-by-genres.strategy');
const FindMoviesByDurationAndGenresStrategy = require('../strategies/find-movies-by-duration-and-genres.strategy');

/**
 * @swagger
 * /movies:
 *  get:
 *    summary: Returns list of movies
 *    tags:
 *      - Movies
 *    parameters:
 *      - in: query
 *        name: genres
 *        required: false
 *        schema:
 *          type: array
 *          items:
 *            type: string
 *      - in: query
 *        name: duration
 *        required: false
 *        schema:
 *          type: integer
 *    responses:
 *      200:
 *        description: List of the movies
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Movie'
 *      500:
 *        description: Internal server error
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                stack:
 *                  type: string
 */
class FindMoviesAction extends BaseAction {
  constructor (container) {
    super('GET', '/movies');

    this.findRandomMovieStrategy = container.get(FindRandomMovieStrategy);
    this.findMovieByDurationStrategy = container.get(FindMovieByDurationStrategy);
    this.findMoviesByGenresStrategy = container.get(FindMoviesByGenresStrategy);
    this.findMoviesByDurationAndGenresStrategy = container.get(FindMoviesByDurationAndGenresStrategy);
  }

  execute = async (req, res, next) => {
    try {
      const genres = req.query.genres;
      const duration = +req.query.duration;

      const movies = await this.#executeStrategy(genres, duration);
      res.json(movies);
    } catch (err) {
      next(err);
    }
  }

  #executeStrategy = async (genres, duration) => {
    let movies;

    if (!genres && !duration) {
      movies = await this.findRandomMovieStrategy.execute();
    }

    if (!genres && duration) {
      movies = await this.findMovieByDurationStrategy.execute(duration);
    }

    if (genres && !duration) {
      movies = await this.findMoviesByGenresStrategy.execute(genres);
    }

    if (genres && duration) {
      movies = await this.findMoviesByDurationAndGenresStrategy.execute(genres, duration);
    }

    return movies;
  }
}

module.exports = FindMoviesAction;
