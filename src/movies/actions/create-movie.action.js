const { Segments, celebrate } = require('celebrate');

const BaseAction = require('../../shared/actions/base.action');
const PostCreateMovieDto = require('../dto/post-create-movie.dto');
const CreateMovieStrategy = require('../strategies/create-movie.strategy');

/**
 * @swagger
 * /movies:
 *  post:
 *    summary: Creates new movie
 *    requestBody:
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  $ref: '#/components/schemas/CreateMovie'
 *    tags:
 *      - Movies
 *    responses:
 *      200:
 *        description: Created movie
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Movie'
 *      400:
 *        description: Bad request body
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                stack:
 *                  type: string
 *                validation:
 *                  type: object
 *                  required: false
 *      500:
 *        description: Internal server error
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                stack:
 *                  type: string
 */
class CreateMovieAction extends BaseAction {
  constructor (container) {
    super('POST', '/movies', [
      celebrate({
        [Segments.BODY]: PostCreateMovieDto.validation
      })
    ]);

    this.createMovieStrategy = container.get(CreateMovieStrategy);
  }

  execute = async (req, res, next) => {
    try {
      const movie = await this.createMovieStrategy.execute(new PostCreateMovieDto(req.body));
      res.status(200).json(movie);
    } catch (err) {
      next(err);
    }
  }
}

module.exports = CreateMovieAction;
