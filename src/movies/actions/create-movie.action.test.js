const Container = require('typedi').Container;
const request = require('supertest');
const mockFs = require('mock-fs');
const path = require('path');

const db = require('../../../data/db.json');
const app = require('../../../app');
const JsonDatabase = require('../../shared/database/json.database');

const filePath = 'testDb.json';

describe('POST /movies', () => {
  beforeEach(() => {
    // Mock filesystem to not overwrite database data during the tests
    mockFs({
      './node_modules': mockFs.load(path.resolve('node_modules')),
      './data/db.json': JSON.stringify(db),
      './src': mockFs.directory({ mode: 0o775 }),
      'testDb.json': JSON.stringify(db)
    });

    Container.set('jsonDatabase', new JsonDatabase(filePath));
  });

  afterEach(() => {
    mockFs.restore();
  });

  it('SHOULD respond with json', (done) => {
    request(app)
      .post('/movies')
      .send(createTestBody('Test title'))
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        expect(res.body.title).toEqual('Test title');
        done();
      });
  });
});

function createTestBody (title, genres) {
  return {
    title,
    year: 1980,
    runtime: 110,
    genres: genres || ['Comedy'],
    director: 'John Doe',
    actors: 'Johnny Doe'
  };
}
