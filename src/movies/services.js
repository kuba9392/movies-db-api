const JsonDatabase = require('../shared/database/json.database');

function init (container) {
  container.set('moviesDatabase', new JsonDatabase(require.resolve('../../data/db.json')));
}

module.exports = {
  init
};
