const Container = require('typedi').Container;

const FindMovieByDurationStrategy = require('./find-movie-by-duration.strategy');
const MoviesRepository = require('../repositories/movies.repository');

describe('FindMovieByDurationStrategy', () => {
  let repository;
  let strategy;

  beforeEach(() => {
    repository = {
      find: async () => {}
    };
    Container.set(MoviesRepository, repository);

    strategy = new FindMovieByDurationStrategy(Container);
  });

  afterEach(() => {
    Container.reset();
  });

  describe('execute method', () => {
    it('SHOULD return random movie in runtime duration from database WHEN duration have been passed', async () => {
      jest.spyOn(repository, 'find').mockResolvedValue([{ testProp: 'testValue', runtime: 10 }, { testProp: 'testValue2', runtime: 15 }]);

      const result = await strategy.execute(10);

      expect(result).toHaveLength(1);
      expect(result[0]).toHaveProperty('testProp');
      expect(result[0]).toHaveProperty('runtime');
    });
  });
});
