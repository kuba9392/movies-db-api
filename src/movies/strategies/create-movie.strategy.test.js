const Container = require('typedi').Container;

const CreateMovieStrategy = require('./create-movie.strategy');
const MoviesRepository = require('../repositories/movies.repository');
const GenresRepository = require('../repositories/genres.repository');
const PostCreateMovieDto = require('../dto/post-create-movie.dto');

describe('CreateMovieStrategy', () => {
  let moviesRepository;
  let genresRepository;
  let strategy;

  beforeEach(() => {
    moviesRepository = {
      save: async () => {}
    };

    genresRepository = {
      find: async () => {}
    };

    Container.set(MoviesRepository, moviesRepository);
    Container.set(GenresRepository, genresRepository);

    strategy = new CreateMovieStrategy(Container);
  });

  afterEach(() => {
    Container.reset();
  });

  describe('execute method', () => {
    it('SHOULD throw error WHEN provided genres does not exists in database', async () => {
      const testDto = createTestDto('test-movie', ['test-genre3']);
      const genres = ['test-genre1', 'test-genre2'];
      jest.spyOn(genresRepository, 'find').mockResolvedValue(genres);
      jest.spyOn(moviesRepository, 'save');

      let errorMessage;
      try {
        await strategy.execute(testDto);
      } catch (err) {
        errorMessage = err.message;
      }

      expect(genresRepository.find).toHaveBeenCalledTimes(1);
      expect(moviesRepository.save).toHaveBeenCalledTimes(0);
      expect(errorMessage).toEqual('One or all genres does not exists in the database');
    });

    it('SHOULD save new movie', async () => {
      const testDto = createTestDto('test-movie', ['test-genre2']);
      const genres = ['test-genre1', 'test-genre2'];
      jest.spyOn(genresRepository, 'find').mockResolvedValue(genres);
      jest.spyOn(moviesRepository, 'save').mockResolvedValue({ ...testDto, id: 1 });

      const result = await strategy.execute(testDto);

      expect(genresRepository.find).toHaveBeenCalledTimes(1);
      expect(moviesRepository.save).toHaveBeenCalledTimes(1);
      expect(result.id).toEqual(1);
    });
  });
});

function createTestDto (title, genres) {
  return new PostCreateMovieDto({
    title,
    year: 1980,
    runtime: 110,
    genres: genres || ['test-genre1'],
    director: 'John Doe',
    actors: 'Johnny Doe',
    plot: undefined,
    posterUrl: undefined
  });
}
