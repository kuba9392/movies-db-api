const Container = require('typedi').Container;

const FindMoviesByDurationAndGenres = require('./find-movies-by-duration-and-genres.strategy');
const MoviesRepository = require('../repositories/movies.repository');

describe('FindMoviesByDurationAndGenres', () => {
  let repository;
  let strategy;

  beforeEach(() => {
    repository = {
      find: async () => {}
    };
    Container.set(MoviesRepository, repository);

    strategy = new FindMoviesByDurationAndGenres(Container);
  });

  afterEach(() => {
    Container.reset();
  });

  describe('execute method', () => {
    it('SHOULD return ordered movies by number of matches between runtime range WHEN genres and duration have been provided', async () => {
      const movies = [
        { genres: ['Thriller', 'Drama'], runtime: 50 },
        { genres: ['Comedy', 'Drama'], runtime: 52 },
        { genres: ['Drama', 'Comedy'], runtime: 56 },
        { genres: ['Drama', 'Comedy', 'Sport'], runtime: 59 }
      ];

      jest.spyOn(repository, 'find').mockResolvedValue(movies);

      const result = await strategy.execute(['Drama', 'Comedy', 'Sport'], 50);

      expect(result).toHaveLength(4);
      expect(result[0].genres).toEqual(['Drama', 'Comedy', 'Sport']);
      expect(result[0].runtime).toEqual(59);
      expect(result[1].genres).toEqual(['Comedy', 'Drama']);
      expect(result[1].runtime).toEqual(52);
      expect(result[2].genres).toEqual(['Drama', 'Comedy']);
      expect(result[2].runtime).toEqual(56);
      expect(result[3].genres).toEqual(['Thriller', 'Drama']);
      expect(result[3].runtime).toEqual(50);
    });
  });
});
