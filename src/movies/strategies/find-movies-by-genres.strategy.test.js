const Container = require('typedi').Container;

const FindMoviesByGenresStrategy = require('./find-movies-by-genres.strategy');
const MoviesRepository = require('../repositories/movies.repository');

describe('FindMoviesByGenresStrategy', () => {
  let repository;
  let strategy;

  beforeEach(() => {
    repository = {
      find: async () => {}
    };
    Container.set(MoviesRepository, repository);

    strategy = new FindMoviesByGenresStrategy(Container);
  });

  afterEach(() => {
    Container.reset();
  });

  describe('execute method', () => {
    it('SHOULD return ordered movies by number of matches WHEN genres have been provided', async () => {
      const movies = [
        { genres: ['Thriller', 'Drama'] },
        { genres: ['Comedy', 'Drama'] },
        { genres: ['Drama', 'Comedy'] },
        { genres: ['Drama', 'Comedy', 'Sport'] }
      ];

      jest.spyOn(repository, 'find').mockResolvedValue(movies);

      const result = await strategy.execute(['Drama', 'Comedy', 'Sport']);

      expect(result).toHaveLength(4);
      expect(result[0].genres).toEqual(['Drama', 'Comedy', 'Sport']);
      expect(result[1].genres).toEqual(['Comedy', 'Drama']);
      expect(result[2].genres).toEqual(['Drama', 'Comedy']);
      expect(result[3].genres).toEqual(['Thriller', 'Drama']);
    });
  });
});
