const MoviesRepository = require('../repositories/movies.repository');
const MoviesUtils = require('../utils/movies.utils');

class FindMoviesByGenresStrategy {
  constructor (container) {
    this.repository = container.get(MoviesRepository);
  }

  /**
   * Find movies by genres
   * @async
   * @param {String[]} genres
   */
  async execute (genres) {
    const movies = await this.repository.find(movie => {
      return MoviesUtils.hasOneOfGenres(movie, genres);
    });

    return MoviesUtils.sortByNumberOfMatches(movies, genres);
  }
}

module.exports = FindMoviesByGenresStrategy;
