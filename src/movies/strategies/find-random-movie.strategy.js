const MoviesRepository = require('../repositories/movies.repository');

class FindRandomMovieStrategy {
  constructor (container) {
    this.repository = container.get(MoviesRepository);
  }

  /**
   * Find random movie
   * @async
   */
  async execute () {
    const movies = await this.repository.find();
    return [movies[Math.floor(Math.random() * movies.length)]];
  }
}

module.exports = FindRandomMovieStrategy;
