const MoviesRepository = require('../repositories/movies.repository');
const MoviesUtils = require('../utils/movies.utils');

class FindMovieByDurationStrategy {
  constructor (container) {
    this.repository = container.get(MoviesRepository);
  }

  /**
   * Find movie by duration
   * @async
   * @param {integer} duration
   */
  async execute (duration) {
    const movies = await this.repository.find(movie => {
      return MoviesUtils.isBetweenDurationRange(movie, duration);
    });

    return [movies[Math.floor(Math.random() * movies.length)]];
  }
}

module.exports = FindMovieByDurationStrategy;
