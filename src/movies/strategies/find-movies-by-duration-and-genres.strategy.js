const MoviesRepository = require('../repositories/movies.repository');
const MoviesUtils = require('../utils/movies.utils');

class FindMoviesByDurationAndGenresStrategy {
  constructor (container) {
    this.repository = container.get(MoviesRepository);
  }

  /**
   * Find movies by duration and genres
   * @async
   * @param {String[]} genres
   * @param {integer} duration
   */
  async execute (genres, duration) {
    const movies = await this.repository.find(movie => {
      return MoviesUtils.hasOneOfGenres(movie, genres) && MoviesUtils.isBetweenDurationRange(movie, duration);
    });

    return MoviesUtils.sortByNumberOfMatches(movies, genres);
  }
}

module.exports = FindMoviesByDurationAndGenresStrategy;
