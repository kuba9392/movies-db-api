const Container = require('typedi').Container;

const FindRandomMovieStrategy = require('./find-random-movie.strategy');
const MoviesRepository = require('../repositories/movies.repository');

describe('FindMoviesStrategy', () => {
  let repository;
  let strategy;

  beforeEach(() => {
    repository = {
      find: async () => {}
    };
    Container.set(MoviesRepository, repository);

    strategy = new FindRandomMovieStrategy(Container);
  });

  afterEach(() => {
    Container.reset();
  });

  describe('execute method', () => {
    it('SHOULD return random movie from database WHEN no params have been passed', async () => {
      jest.spyOn(repository, 'find').mockResolvedValue([{ testProp: 'testValue' }]);

      const result = await strategy.execute();

      expect(result).toHaveLength(1);
      expect(result[0].testProp).toEqual('testValue');
    });
  });
});
