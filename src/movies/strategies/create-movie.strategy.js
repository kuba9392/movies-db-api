const MoviesRepository = require('../repositories/movies.repository');
const GenresRepository = require('../repositories/genres.repository');
const Movie = require('../entities/movie');
const HttpError = require('../../shared/errors/http.error');

class CreateMovieStrategy {
  constructor (container) {
    this.moviesRepository = container.get(MoviesRepository);
    this.genresRepository = container.get(GenresRepository);
  }

  /**
   * Movie creation strategy
   *
   * @async
   * @function execute
   * @param {PostCreateMovieDto} dto | Data Transfer Object with movie data
   */
  async execute (dto) {
    const genres = await this.genresRepository.find();

    if (!dto.genres.every(genre => genres.includes(genre))) {
      throw new HttpError(400, 'One or all genres does not exists in the database');
    }

    const movie = Movie.createFromDto(dto);
    return this.moviesRepository.save(movie);
  }
}

module.exports = CreateMovieStrategy;
