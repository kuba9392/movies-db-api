const log4js = require('log4js');
const loggerConfig = require('./config/log4js.json');

if (process.env.NODE_ENV !== 'test') {
  log4js.configure(loggerConfig);
}

module.exports = log4js.getLogger();
